package com.springdemo.employee;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/employee/empId")
public class EmployeeController {

    @GetMapping("/{empId}")
    public String getEmployeeById(@PathVariable("empId") String empId){
        return "Employee details for employee id = "+ empId + " are not found";
    }
}
